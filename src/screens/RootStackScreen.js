import React from 'react';


import { createNativeStackNavigator } from '@react-navigation/native-stack';


import SplashScreen from './SplashScreen';
import SignInScreen from './SignInScreen';
import SignUpScreen from './SignUpScreen';
import HomeScreen from './HomeScreen'
import MainTabScreen from './MainTabScreen'

const RootStack = createNativeStackNavigator();

const RootStackScreen = ({navigation}) => (
    <RootStack.Navigator headerMode='none'>
        <RootStack.Screen name="SplashScreen" options={{headerShown:false}} component={SplashScreen}/>
        <RootStack.Screen name="SignInScreen" options={{headerShown:false}} component={SignInScreen}/>
        <RootStack.Screen name="SignUpScreen" options={{headerShown:false}} component={SignUpScreen}/>
        <RootStack.Screen name="HomeScreen" options={{headerShown:false}} component={HomeScreen}/>
        <RootStack.Screen name="MainTabScreen" options={{headerShown:false}} component={MainTabScreen}/>
    </RootStack.Navigator>
);

export default RootStackScreen;